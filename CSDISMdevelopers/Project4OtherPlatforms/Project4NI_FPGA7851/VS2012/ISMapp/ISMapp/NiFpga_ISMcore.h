/*
 * Generated with the FPGA Interface C API Generator 14.0.0
 * for NI-RIO 14.0.0 or later.
 */

#ifndef __NiFpga_ISMcore_h__
#define __NiFpga_ISMcore_h__

#ifndef NiFpga_Version
   #define NiFpga_Version 1400
#endif

#include "NiFpga.h"

/**
 * The filename of the FPGA bitfile.
 *
 * This is a #define to allow for string literal concatenation. For example:
 *
 *    static const char* const Bitfile = "C:\\" NiFpga_ISMcore_Bitfile;
 */
#define NiFpga_ISMcore_Bitfile "NiFpga_ISMcore.lvbitx"

/**
 * The signature of the FPGA bitfile.
 */
static const char* const NiFpga_ISMcore_Signature = "4A6CD59461E4083CC413E0CA345DF3CC";

typedef enum
{
   NiFpga_ISMcore_IndicatorBool_CameraTrigger = 0x815A,
   NiFpga_ISMcore_IndicatorBool_Finished = 0x815E,
   NiFpga_ISMcore_IndicatorBool_LaserTrigger = 0x8156,
   NiFpga_ISMcore_IndicatorBool_Signal_in = 0x8112,
} NiFpga_ISMcore_IndicatorBool;

typedef enum
{
   NiFpga_ISMcore_IndicatorI16_OutputImages = 0x813E,
} NiFpga_ISMcore_IndicatorI16;

typedef enum
{
   NiFpga_ISMcore_ControlBool_EnL1 = 0x811A,
   NiFpga_ISMcore_ControlBool_EnL2 = 0x8122,
   NiFpga_ISMcore_ControlBool_EnL3 = 0x811E,
   NiFpga_ISMcore_ControlBool_EnL4 = 0x8116,
   NiFpga_ISMcore_ControlBool_Enable = 0x816A,
   NiFpga_ISMcore_ControlBool_Start = 0x8152,
} NiFpga_ISMcore_ControlBool;

typedef enum
{
   NiFpga_ISMcore_ControlI16_DiskCyclesPerImage = 0x8166,
   NiFpga_ISMcore_ControlI16_ExpectedImages = 0x813A,
   NiFpga_ISMcore_ControlI16_LaserIntensity1 = 0x8142,
   NiFpga_ISMcore_ControlI16_LaserIntensity2 = 0x812E,
   NiFpga_ISMcore_ControlI16_LaserIntensity3 = 0x812A,
   NiFpga_ISMcore_ControlI16_LaserIntensity4 = 0x8126,
   NiFpga_ISMcore_ControlI16_M1 = 0x8136,
   NiFpga_ISMcore_ControlI16_M2 = 0x8132,
   NiFpga_ISMcore_ControlI16_NumofLaserPulse = 0x8146,
   NiFpga_ISMcore_ControlI16_PulseShiftOffset = 0x8162,
   NiFpga_ISMcore_ControlI16_PulseWidth = 0x810E,
} NiFpga_ISMcore_ControlI16;

#endif
