//     SDC-ISM MicroManager Plugin: A open source software for Spinning Disk Confocal-Image Scanning microscopy image acquisition
//
//     Copyright (C) 2019  Shun Qin, shun.qin@phys.uni-goettingen.de
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//     We expect you would cite our article, to which this source code project attach.



package uniGoettingen.DPI.ism;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.imageio.ImageIO;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.JComponent;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Color;

import javax.swing.JTextPane;
import javax.swing.SwingConstants;

import org.micromanager.api.ScriptInterface;
import org.micromanager.api.SequenceSettings;
import org.micromanager.dialogs.AcqControlDlg;
import org.micromanager.utils.MMScriptException;
import org.micromanager.utils.ReportingUtils;

import ij.IJ;
import ij.ImagePlus;
import ij.process.ImageProcessor;
import mmcorej.CMMCore;
import mmcorej.StrVector;
import mmcorej.TaggedImage;

import java.awt.event.InputMethodListener;
import java.awt.event.InputMethodEvent;

import javax.swing.JEditorPane;
import javax.swing.JTextField;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferUShort;
import java.awt.image.Raster;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.swing.JTextArea;
import javax.swing.JLabel;

import java.awt.Canvas;

import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.TitledBorder;
import javax.swing.JSplitPane;
import javax.swing.JComboBox;

import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import javax.swing.JTabbedPane;
import java.awt.SystemColor;
import java.awt.Toolkit;

import javax.swing.DropMode;
import java.awt.Component;
import javax.swing.JCheckBox;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JSlider;

public class ISMControlGUI {

	JFrame frame;
	private JTextField TextField_Wp;
	private JTextField TextField_Nc;
	private JTextField TextField_Np;
	private JTextField TextField_Ni;
	private JLabel Label_ExpectedImageNum;
	private JLabel Label_SpinningCyclesImage;
	private JLabel Label_LaserPulseNum;
	private JLabel lblLaserPulseWideus;
	private Thread thread;
	CMMCore core;
	ScriptInterface app;
	
	Drawpanel drawpanel = new Drawpanel(null);//"C:\\Users\\Administrator\\Pictures\\20170123_113924.jpg"
	private JRadioButton RadioButton_L4;
	private JRadioButton RadioButton_L1;
	private JRadioButton RadioButton_L3;
	private JRadioButton RadioButton_L2;
	protected ButtonGroup bGroup;
	private JTextField textField_Nimg;
	private JTextField textField_M1;
	private JLabel lblDelayBefore;
	private JTextField textField_M2;
	private JLabel lblDelayAfter;
	private int color = 1;
	private JSlider JSlider_I1;
	private JSlider JSlider_I3;
	private JSlider JSlider_I4;
	private JSlider JSlider_I2;
	private JLabel lblClock;
	private JTextField textField_clk;
	private JPanel panel_Laser;
	private JPanel panel_FPGA;
	private JPanel panel_1;
	private JPanel panel_ImageView;
	private boolean stopthread = false;
	ISMPlugin ismPlugin = null;
	//ISMImageProcessorGUI ismImageProcessor = null;
	String acqname = null;
	String LastTriggerMod = "Software";
	String LastFrameTransferState = "Off";
	SequenceSettings seqsetting;
	String Camera = null;
	String[] LastHamCamMod = {"","",""};
	int Nrun = 0;
	int Imax = 65535; // maximum input for 16 bits FPGA inner D/A converter  
	
	/**
	 * Launch the application.
	 */
	
	//private Thread thread2;
	private boolean appstop = false;
	private boolean isfirstacq = true;
	private boolean isfirszsectionging = true;
	boolean acqfinished = true;
	
	boolean EnL1 = false, EnL2 = false, EnL3 = false, EnL4 = false;
	int IL1 = 20000, IL2 = 0, IL3 = 0, IL4 =0;
	int Ni = 0, Np = 0;
	int Nc = 0;
	int M1 = 0, M2 = 1, Nwp = 0;
	double Wp = 0;
	int Fclk;
	
	double zstep = 0;
	double zstart = 0;
	double zend = 0;	
	int nslice = 1;
	
	private JButton ImageProButton;
	private JTextField Zstart_textField;
	private JTextField Zend_textField;
	private JTextField Zstep_textField;
	private JLabel lbl_Zposition;
	private JLabel lbl_Nslice;

	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ISMControlGUI window = new ISMControlGUI(null, null, null);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ISMControlGUI(ScriptInterface cmm_app, CMMCore cmm_core, ISMPlugin ism_Plugin) {
		
		core = cmm_core;
		app = cmm_app;
		ismPlugin = ism_Plugin;
		initialize();	
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 1098, 672);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		System.loadLibrary("ISMcore"); 
		
		frame.getContentPane().setLayout(null);
		
		frame.setTitle("The Third Institute of Physics-University of G�ttingen");
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("icon_img.png")));
		
		drawpanel.setBounds(417, 55, 1000, 1000);
		//frame.getContentPane().add(drawpanel);
		frame.getContentPane().add(drawpanel, BorderLayout.CENTER);
		frame.repaint();
		
		
		bGroup = new ButtonGroup();
		
		lblClock = new JLabel("Clock/MHz:");
		lblClock.setBounds(32, 38, 87, 18);
		frame.getContentPane().add(lblClock);
		
		textField_clk = new JTextField();
		textField_clk.setBounds(105, 35, 106, 24);
		frame.getContentPane().add(textField_clk);
		textField_clk.setColumns(10);
		textField_clk.setText("40");
		
		panel_Laser = new JPanel();
		panel_Laser.setBorder(new TitledBorder(null, "Laser Channel", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_Laser.setBounds(30, 70, 395, 89);
		frame.getContentPane().add(panel_Laser);
		panel_Laser.setLayout(null);
		
		RadioButton_L1 = new JRadioButton("Ch1");
		RadioButton_L1.setBackground(SystemColor.menu);
		RadioButton_L1.setForeground(Color.BLACK);
		RadioButton_L1.setBounds(18, 20, 61, 27);
		panel_Laser.add(RadioButton_L1);
		RadioButton_L1.setActionCommand("1");
		
		RadioButton_L1.addActionListener(new ActionListener(){
	        @Override
	        public void actionPerformed(ActionEvent e) {
	        	ButtonModel i = bGroup.getSelection();
	    		System.out.println("Selected Button: "+1);	           

	        }
	    });
		//bGroup.add(CheckBox_L1);
		
		JSlider_I1 = new JSlider();
		JSlider_I1.setMaximum(32767);
		JSlider_I1.setBounds(91, 21, 111, 24);
		panel_Laser.add(JSlider_I1);
		
		RadioButton_L2 = new JRadioButton("Ch2");
		RadioButton_L2.setBackground(SystemColor.menu);
		RadioButton_L2.setBounds(208, 20, 61, 27);
		panel_Laser.add(RadioButton_L2);
		RadioButton_L2.setActionCommand("2");
		//bGroup.add(CheckBox_L2);
		
		JSlider_I2 = new JSlider();
		JSlider_I2.setMaximum(32767);
		JSlider_I2.setBounds(262, 21, 111, 24);
		panel_Laser.add(JSlider_I2);
		
		RadioButton_L3 = new JRadioButton("Ch3");
		RadioButton_L3.setBackground(SystemColor.menu);
		RadioButton_L3.setBounds(18, 54, 61, 27);
		panel_Laser.add(RadioButton_L3);
		RadioButton_L3.setActionCommand("3");
		//bGroup.add(CheckBox_L3);
		
		RadioButton_L4 = new JRadioButton("Ch4");
		RadioButton_L4.setSelected(true);
		RadioButton_L4.setBackground(SystemColor.menu);
		RadioButton_L4.setBounds(208, 54, 61, 27);
		panel_Laser.add(RadioButton_L4);
		RadioButton_L4.setActionCommand("4");
		//bGroup.add(CheckBox_L4);
		
		JSlider_I4 = new JSlider();
		JSlider_I4.setMaximum(32767);
		JSlider_I4.setBounds(262, 55, 111, 24);
		panel_Laser.add(JSlider_I4);
		
		JSlider_I3 = new JSlider();
		JSlider_I3.setMaximum(32767);
		JSlider_I3.setBounds(91, 55, 111, 24);
		panel_Laser.add(JSlider_I3);
		
		panel_FPGA = new JPanel();
		panel_FPGA.setForeground(Color.BLUE);
		panel_FPGA.setBorder(new TitledBorder(null, "FPGA", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_FPGA.setBounds(30, 291, 395, 252);
		frame.getContentPane().add(panel_FPGA);
		panel_FPGA.setLayout(null);
		
		TextField_Ni = new JTextField();
		TextField_Ni.setBounds(209, 25, 63, 24);
		panel_FPGA.add(TextField_Ni);
		TextField_Ni.setColumns(10);
		TextField_Ni.setText("250");
		
		Label_ExpectedImageNum = new JLabel("Expected Image Number:");
		Label_ExpectedImageNum.setBounds(28, 28, 176, 18);
		panel_FPGA.add(Label_ExpectedImageNum);
		
		Label_LaserPulseNum = new JLabel("Laser Pulses Number:");
		Label_LaserPulseNum.setBounds(28, 57, 160, 18);
		panel_FPGA.add(Label_LaserPulseNum);
		
		TextField_Np = new JTextField();
		TextField_Np.setBounds(209, 54, 143, 24);
		panel_FPGA.add(TextField_Np);
		TextField_Np.setColumns(10);
		TextField_Np.setText("4");
		
		TextField_Wp = new JTextField();
		TextField_Wp.setBounds(209, 82, 143, 24);
		panel_FPGA.add(TextField_Wp);
		TextField_Wp.setColumns(10);
		TextField_Wp.setText("6");
		
		lblLaserPulseWideus = new JLabel("Laser Pulse Width(us):");
		lblLaserPulseWideus.setBounds(28, 86, 176, 18);
		panel_FPGA.add(lblLaserPulseWideus);
		
		
		lblDelayBefore = new JLabel("Camera activation time(us):");
		lblDelayBefore.setBounds(28, 144, 168, 18);
		panel_FPGA.add(lblDelayBefore);
		
		TextField_Nc = new JTextField();
		TextField_Nc.setBounds(209, 112, 143, 24);
		panel_FPGA.add(TextField_Nc);
		TextField_Nc.setColumns(10);
		TextField_Nc.setText("20");
		
		textField_M1 = new JTextField();
		textField_M1.setBounds(209, 143, 143, 24);
		panel_FPGA.add(textField_M1);
		textField_M1.setColumns(10);
		textField_M1.setText("10");
		
		Label_SpinningCyclesImage = new JLabel("Spinning Cycles/Image:");
		Label_SpinningCyclesImage.setBounds(28, 115, 176, 18);
		panel_FPGA.add(Label_SpinningCyclesImage);
		
		lblDelayAfter = new JLabel("Read out delay(Spin-cycle):");
		lblDelayAfter.setBounds(28, 173, 160, 18);
		panel_FPGA.add(lblDelayAfter);
		
		textField_M2 = new JTextField();
		textField_M2.setBounds(209, 173, 143, 24);
		panel_FPGA.add(textField_M2);
		textField_M2.setColumns(10);
		textField_M2.setText("25");
		
		JButton StopButton = new JButton("Stop");
		StopButton.setBounds(219, 202, 122, 27);
		panel_FPGA.add(StopButton);
				
		final JButton StartButton = new JButton("start");
		StartButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		StartButton.setBounds(28, 202, 112, 27);
		panel_FPGA.add(StartButton);
		//StartButton.setBackground(SystemColor.menu);
		
		//number of output image
		textField_Nimg = new JTextField();
		textField_Nimg.setForeground(Color.RED);
		textField_Nimg.setHorizontalAlignment(SwingConstants.LEFT);
		textField_Nimg.setFont(new Font("Tahoma", Font.PLAIN, 11));
		textField_Nimg.setBounds(282, 25, 70, 24);
		panel_FPGA.add(textField_Nimg);
		textField_Nimg.setColumns(10);
		textField_Nimg.disable();
		textField_Nimg.setText("0");
		textField_Nimg.setDisabledTextColor(Color.RED);
		
		drawpanel.setBounds(9, 15, 535, 529);
		frame.getContentPane().add(drawpanel);
		//frame.getContentPane().add(drawpanel, BorderLayout.CENTER);
		drawpanel.setBackground (Color.BLACK);
		frame.repaint();
		
		panel_ImageView = new JPanel();
		panel_ImageView.setBorder(new TitledBorder(null, "Image View", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_ImageView.setBounds(480, 69, 553, 553);
		frame.getContentPane().add(panel_ImageView);
		panel_ImageView.setLayout(null);
		
		panel_ImageView.add(drawpanel);
		
		JLabel lblLabel_Title = new JLabel("Spinning Disk Confocal-Image Scanning Microscope Control Panel");
		lblLabel_Title.setHorizontalAlignment(SwingConstants.CENTER);
		lblLabel_Title.setFont(new Font("Trebuchet MS", Font.BOLD, 18));
		lblLabel_Title.setBounds(252, 11, 581, 30);
		frame.getContentPane().add(lblLabel_Title);
		
		JPanel panel_Message = new JPanel();
		panel_Message.setBorder(new TitledBorder(null, "Message", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_Message.setBounds(30, 554, 395, 68);
		frame.getContentPane().add(panel_Message);
		panel_Message.setLayout(null);
		
		final JTextArea TextArea_Message = new JTextArea();
		TextArea_Message.setWrapStyleWord(true);
		TextArea_Message.setEditable(false);
		TextArea_Message.setLineWrap(true);
		TextArea_Message.setColumns(10);
		TextArea_Message.setDropMode(DropMode.INSERT);
		TextArea_Message.setBounds(8, 14, 377, 45);
		panel_Message.add(TextArea_Message);
		/*
		ImageProButton = new JButton("Image Processor");
		ImageProButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(ismImageProcessor==null){
					ismImageProcessor = new ISMImageProcessorGUI(ISMControlGUI.this);
					ismImageProcessor.setLocationRelativeTo(null);
					
				}
				ismImageProcessor.setVisible(true);
			}
		});
		ImageProButton.setBounds(4, 0, 153, 27);
		frame.getContentPane().add(ImageProButton);
		*/
		JPanel panel_Zsection = new JPanel();
		panel_Zsection.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				String zpos;
				try {
					zpos = "Current Position:"+String.format("%.3f", core.getPosition());
					lbl_Zposition.setText(zpos);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				lbl_Zposition.setText("Current Position:");
			}
		});
		panel_Zsection.setBorder(new TitledBorder(null, "Z-Sectioning", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_Zsection.setBounds(30, 170, 395, 106);
		frame.getContentPane().add(panel_Zsection);
		panel_Zsection.setLayout(null);
		
		JLabel lbl_Zstart = new JLabel("z-start:");
		lbl_Zstart.setBounds(16, 62, 41, 14);
		panel_Zsection.add(lbl_Zstart);
		
		JLabel lbl_Zend = new JLabel("z-end:");
		lbl_Zend.setBounds(282, 62, 36, 14);
		panel_Zsection.add(lbl_Zend);
		
		Zstep_textField = new JTextField();
		Zstep_textField.setText("0.5");
		Zstep_textField.setBounds(192, 59, 64, 20);
		panel_Zsection.add(Zstep_textField);
		Zstep_textField.setColumns(10);
		
		JLabel lblZstep = new JLabel("z-step:");
		lblZstep.setBounds(147, 62, 41, 14);
		panel_Zsection.add(lblZstep);
		
		Zend_textField = new JTextField();
		Zend_textField.setText("55");
		Zend_textField.setBounds(321, 59, 64, 20);
		panel_Zsection.add(Zend_textField);
		Zend_textField.setColumns(10);
		
		Zstart_textField = new JTextField();
		Zstart_textField.setText("50");
		Zstart_textField.setToolTipText("");
		Zstart_textField.addInputMethodListener(new InputMethodListener() {
			public void caretPositionChanged(InputMethodEvent arg0) {
			}
			public void inputMethodTextChanged(InputMethodEvent arg0) {
			}
		});
		Zstart_textField.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {		
				
			}
		});
		Zstart_textField.setBounds(64, 59, 63, 20);
		panel_Zsection.add(Zstart_textField);
		Zstart_textField.setColumns(10);
		
		final JRadioButton rdbtn_Zsectioning = new JRadioButton("Enable");
		rdbtn_Zsectioning.setBounds(16, 26, 63, 23);
		panel_Zsection.add(rdbtn_Zsectioning);
		rdbtn_Zsectioning.setToolTipText("");
		
		lbl_Zposition = new JLabel("Current Position:");
		lbl_Zposition.setBounds(85, 30, 133, 14);
		panel_Zsection.add(lbl_Zposition);
		
		lbl_Nslice = new JLabel("");
		lbl_Nslice.setBounds(222, 30, 163, 14);
		panel_Zsection.add(lbl_Nslice);
		StartButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				StartButton.setEnabled(false);
				StartButton.setBackground(Color.GRAY);
				Nrun = Nrun + 1;
				//clear image view

				textField_Nimg.setText("0");
				
				ImagePlus imgp = IJ.createImage("My new image", "16-bit black", 512, 512, 1);  
				drawpanel.image = imgp.getBufferedImage();				
				frame.repaint();
				
				//start imaging
				String strNi = TextField_Ni.getText();
				String strNp = TextField_Np.getText();
				String strNc = TextField_Nc.getText();
				String strWp = TextField_Wp.getText();
				String strM1 = textField_M1.getText();
				String strM2 = textField_M2.getText();
				String strclk = textField_clk.getText();
				

				
				//String buttoncmd = bGroup.getSelection().getActionCommand();
				// get current value for channel 1
				IL1 = JSlider_I1.getValue();
				EnL1 = RadioButton_L1.isSelected();
				// get current value for channel 2
				IL2 = JSlider_I2.getValue();
				EnL2 = RadioButton_L2.isSelected();
				// get current value for channel 3
				IL3 = JSlider_I3.getValue();
				EnL3 = RadioButton_L3.isSelected();
				// get current value for channel 4
				IL4 = JSlider_I4.getValue();
				EnL4 = RadioButton_L4.isSelected();

			
					
				Ni = Integer.parseInt(strNi);
				Np = Integer.parseInt(strNp);
				Nc = Integer.parseInt(strNc);				
				M1 = Integer.parseInt(strM1);
				M2 = Integer.parseInt(strM2);
				Fclk = Integer.parseInt(strclk);	
				Wp = Double.parseDouble(strWp);
				Nwp = (int) (Wp*Fclk);
				
				try{	
					seqsetting = app.getAcquisitionSettings();
				}catch (Exception e) {
					System.out.println(e.getMessage());
					//TextArea_Message.setText("Acquisition settings could not be loaded. Please un-check \"Z-Stacks\" in the Multi-D Acq Panel.");
					JOptionPane.showMessageDialog(null, "z-stacks setting cannot be all 0!\nTo continue, please un-checked \"Z-Stacks\" option in the \"Multi-D Acq.\" panel of MicroManager.");
					StartButton.setEnabled(true);
					return;
					
				}
					
				
				if(rdbtn_Zsectioning.isSelected()){	
					//z-sectioning parameter
					zstep = Double.parseDouble(Zstep_textField.getText());
					zstart = Double.parseDouble(Zstart_textField.getText());
					zend = Double.parseDouble(Zend_textField.getText());	
					nslice = 1 + (int)Math.abs((zstart - zend) / zstep);
					seqsetting.numFrames = nslice*Ni;					
					lbl_Nslice.setText("Total Number of Slices:"+nslice);
//					    seqsetting.slices=[1 1 ];
				}else{
					//set sequenc acquisition protperty
					
				    seqsetting.numFrames = Ni;
				    seqsetting.intervalMs = 0;
				    
				}

				try { 
					seqsetting.slices = new ArrayList<Double>();   
					app.setAcquisitionSettings(seqsetting);
					//core_.setProperty("Andor", "Trigger", "Software");  
					//core_.setProperty("Andor", "PixelType", "16bit");
				}catch (Exception e) {
					// TODO Auto-generated catch block
					System.out.println(e.getMessage());
					TextArea_Message.setText("Input values are not valid!");
					System.out.println("Input values are not valid!");
				}
									
					
				//TriggerMod
				if(Nrun==1) {
					setCameraMode();
				}
					
				//lvFPGA.ChangeParameter(Ni, Nc, IL1, IL2, IL3, IL4, M1, M2, Np, Nwp);
				//System.out.println("Current trigger mode is"+ core.getProperty("Andor", "Trigger"));
				
				
				//run an acquisition in a thread
				Thread AcqThread = new Thread(){
				    public void run(){
				    	
					    try {
							ISMImageAnalyzer imgprocessor = new ISMImageAnalyzer(app.getAcquisitionSettings(), textField_Nimg);
							app.addImageProcessor(imgprocessor);
							
							acqname = app.runAcquisition();	
							acqfinished = true;
							TextArea_Message.setText("Acquisition finished!\n"+"Total Camera Trigger Number: "+lvFPGA.GetNumImageTaken());
							
							//show average intensity after acquisition
							TaggedImage aveimg = imgprocessor.getAveImage();
							if(aveimg != null){
								int width = (int) core.getImageWidth();
								int height = (int) core.getImageHeight();
								
								//app.addToAlbum(aveimg);
								
								ImagePlus imp = IJ.createImage("My new image", "16-bit black", width, height, 1);  
								ImageProcessor ip = imp.getProcessor();
								ip.setPixels(aveimg.pix);
								 
								IJ.run(imp, "Enhance Contrast", "saturated=0.5 normalize");
								//imp.show(); 
								
								drawpanel.image = imp.getBufferedImage();
								frame.repaint();
								imp = null;	
							}
							// reset camera trigger mode 
							//resetCameraMode();
								
							StartButton.setEnabled(true);
							StartButton.setBackground(Color.GREEN);	
							
							app.removeImageProcessor(imgprocessor);
							imgprocessor = null;
							
						} catch (MMScriptException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
				};
				
				AcqThread.start(); 
				
				
				
				// Activate (FPGA) triggering in another thread
				Thread TrigThread = new Thread(){
				    public void run(){
				    	acqfinished = false;
							try {	
							   //core.startContinuousSequenceAcquisition(200);
							   //core.startSequenceAcquisition("Andor", 250, 0, false);
							   //core.startExposureSequence("Andor");
							   //core.displaySLMImage(slmLabel);
							   //lvFPGA.ChangeParameter(Ni, Nc, IL1, IL2, IL3, IL4, M1, M2, Np, Nwp);
											 //ChangeParameter(PNi, PNc, PL1, PL2,  PL3, PL4, PM1, PM2, PNp, PNwp);
							
							   //System.out.println(acqname+"is initializing...");
							   //core.setSLMImage(slmLabel, pixels);
							   //core.
							    //if(acqname!=null && app.getAcquisition(acqname).isInitialized()){// && app.getAcquisitionEngine2010().isRunning()
								while(!core.isSequenceRunning()){	
									
									TextArea_Message.setText("Acquisition is initializing...");
								}
								core.waitForDevice(Camera);
								
								TextArea_Message.setText("Acquisition is starting, please wait...");
								
								if(isfirstacq){
									core.sleep(5000); // in the first acquisition, it take long time to initialize the acquisition engine
									isfirstacq = false;
								}
								else
								{
									//core.sleep(800);
									core.sleep(2000);
								}
								
								
								
								boolean ret = false;
								if(rdbtn_Zsectioning.isSelected()){	
									
									if(isfirszsectionging){
										core.sleep(3500);
										isfirszsectionging = false;
									}
								//boolean isfirst=true;
									if(zstep<0.0000000001 || zstart<0 || zend<0 || zstart>zend){
										app.closeAllAcquisitions();
										return;
									}
									for(int k=0; k<nslice; k++){
										
										if(acqfinished){
											break;
										}
										
										double position = zstart+k*zstep;
										core.setPosition(position);
				  						double ep = 0;
				 						while(ep>0.01){
				 							ep = Math.abs(core.getPosition()-position);
				 						}
										
										core.sleep(1000);
										String stageLabel = core.getAutoFocusDevice();
										if(!stageLabel.isEmpty()){
											core.waitForDevice(stageLabel);
										}
										
										
										String stageLabel1= core.getFocusDevice();
										if(!stageLabel1.isEmpty()){
											core.waitForDevice(stageLabel1);
										}
										
										
										System.out.println("Current Position is: "+core.getPosition());//	+core.getFocusDevice()				
										ret = lvFPGA.StartImaging(Ni, Nc, EnL1, EnL2, EnL3, EnL4, IL1, IL2, IL3, IL4, M1, M2, Np, Nwp,k==0);
										
										if(!ret){
											TextArea_Message.setText("FPGA device does not work, check your NI-FPGA device and driver!");
											app.closeAllAcquisitions();
											return;
										}
										
										core.sleep(1000);
										int Nimg = 0;
										int t = 0;
										while(Nimg<Ni && !appstop && !acqfinished){
											Nimg = lvFPGA.GetNumImageTaken();
											TextArea_Message.setText("Camera Trigger Number = "+Nimg);
											System.out.println("Camera trigger number: "+Nimg);
											core.sleep(100);
											t++;
											if(t>=20 && Nimg<=0){ //avoid endless loop
												TextArea_Message.setText("Fail to call FPGA!");
												break;
											}
										}
										
									}
								
								
								}else{
									
									ret = lvFPGA.StartImaging(Ni, Nc, EnL1, EnL2, EnL3, EnL4,IL1, IL2, IL3, IL4, M1, M2, Np, Nwp,true);
									if(!ret){
										TextArea_Message.setText("FPGA device does not work, check your NI-FPGA device and driver!");
										app.closeAllAcquisitions();
										return;
										
									}
									int Nimg = 0;
									int t = 0;
									while(Nimg<Ni && !appstop && !acqfinished){
										Nimg = lvFPGA.GetNumImageTaken();
										TextArea_Message.setText("Camera Trigger Number = "+Nimg);
										System.out.println("Camera trigger number: "+Nimg);
										core.sleep(100);
										t++;
										if(t>=20 && Nimg<=0){ //avoid endless loop
											TextArea_Message.setText("Fail to call FPGA!");
											break;
										}
									}

								}
								
								
			

							//lvFPGA.CloseImaging();		
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							//String IsmPropty = core_.getProperty("Andor Camera", "Exposure");
							
				
				    }
				};
				TrigThread.start();
				
				
			    Thread thread2 = new Thread(){
				    public void run(){
				    	int Nimg,N0=0;		 
				    	
						while(!appstop && !acqfinished){
							
							//System.out.println("remain count:"+core.getRemainingImageCount());
							
					    	Nimg = lvFPGA.GetNumImageTaken();			    	
					    	if(Nimg != N0){
					    		   TextArea_Message.setText("Camera Trigger Number = "+Nimg);
					    		   //System.out.println("Camera Trigger Number = "+Nimg);
						    	   N0 = Nimg;
					    		}
					    	
					    	//System.out.println(Nimg);
				    		}
					    }
					};
					//thread2.start(); 
				
			}			
		});
		StopButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {	
				
				lvFPGA.StopImaging();
				acqfinished = true;
				thread = null;
				try {
					//core.stopSequenceAcquisition();
					app.closeAllAcquisitions();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// reset camera trigger mode 
				//resetCameraMode();
				TextArea_Message.setText("Camera Trigger Number = "+lvFPGA.GetNumImageTaken());
				//lvFPGA.CloseImaging();
				StartButton.setEnabled(true);
				System.gc();
				//drawpanel.changeImage("C:\\Users\\Administrator\\Pictures\\20170123_114102.jpg");
				//frame.repaint();
			}
		});
		StopButton.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				//disable FPGA to save energy

			}
		});
		
		

		
		
		
		frame.addWindowListener(new java.awt.event.WindowAdapter() {
	        public void windowClosing(WindowEvent winEvt) {
	        	appstop = true;
	        	lvFPGA.CloseImaging();
	        	// reset camera trigger mode 
				resetCameraMode();
	        	if(ismPlugin!=null){
	        		ismPlugin.ISMControl = null;
	        	}
	        		
	        	
	        	
	        	
	            //System.exit(0);
	        }
	    });
		
		
		try{
			
			Camera = core.getCameraDevice();
		}catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		

		/*
		if(Camera.equals("HamamatsuHam_DCAM"))
		{
			StrVector properties;
			try {
				properties = core.getDevicePropertyNames("HamamatsuHam_DCAM");
				for (int i=0; i<properties.size(); i++){
					System.out.println(properties.get(i));


				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
		
		//check if camera is available
		if(Camera.equals("Andor"))
		{
			//core_.loadDevice("Andor", "Andor", "Andor");
			//core_.initializeDevice("Andor");
			Camera = "Andor";
		}*/
		
	}
	
	void setCameraMode() {
		// set the trigger mode of the camera correctly
		try {

			if(Camera.startsWith("Andor")){
				LastTriggerMod = core.getProperty(Camera, "Trigger");
				LastFrameTransferState = core.getProperty(Camera, "FrameTransfer");
				core.setProperty(Camera, "Trigger", "External Exposure");// 
				core.setProperty(Camera, "FrameTransfer", "Off");// 
				//core.setProperty(Camera, "BitDepth", "16");//
			}
			
			if(Camera.startsWith("HamamatsuHam")){
				
				LastHamCamMod[0] = core.getProperty(Camera, "TriggerPolarity");						
				LastHamCamMod[1] = core.getProperty(Camera, "TRIGGER ACTIVE");
				LastHamCamMod[2] = core.getProperty(Camera, "TRIGGER SOURCE");
				
				core.setProperty(Camera, "TriggerPolarity", "POSITIVE");
				core.setProperty(Camera, "TRIGGER SOURCE", "EXTERNAL");
				if(!core.getProperty(Camera, "TRIGGER ACTIVE").equals("LEVEL")){
					core.setProperty(Camera, "TRIGGER ACTIVE", "LEVEL");
				}						
				
			}

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}
	void resetCameraMode() {
		//reset the camera mode
		try {
			
			if(Camera.contains("Andor")){
					core.setProperty(Camera, "Trigger", LastTriggerMod);	
					core.setProperty(Camera, "FrameTransfer", LastFrameTransferState);// 
			}
		
		
			if(Camera.contains("HamamatsuHam") && !LastHamCamMod[0].isEmpty()){	
			core.setProperty(Camera, "OUTPUT TRIGGER POLARITY[0]", LastHamCamMod[0]);
			core.setProperty(Camera, "TRIGGER ACTIVE", LastHamCamMod[1]);
			core.setProperty(Camera, "TRIGGER SOURCE", LastHamCamMod[2]);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	class Drawpanel extends JPanel{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private BufferedImage image;
		public Drawpanel(String path){
			//panel.setBounds(417, 54, 150, 150);

			changeImage(path);
			//Graphics graphic = image.getGraphics();
			//paintComponent(graphic);
			
	
		}
		@Override
		protected void paintComponent(Graphics g){
			super.paintComponent(g);
			g.drawImage(image, 0, 0, null);			
			
		}
		
		protected void changeImage(String path){
			if(path == null){
				return;
			}
			try {
				image = ImageIO.read(new File(path));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
		    }
		}
		
	}	
	
	
	   public boolean checkDev(String DevName){
		    StrVector devices = core.getLoadedDevices();
			for (int i=0; i<devices.size(); i++){
				if(devices.get(i).equals(DevName))
					return true;

			}
			return false;
		   
	   }
}