package uniGoettingen.DPI.ism;


public class FPGAtest{
static boolean stop = false;
//*********** Set environment variable for JDK**********
// sudo update-alternatives --config java
// sudo nano /etc/environment to edit environment file
// add JAVA_HOME="YOUR_PATH"  as last line
// source /etc/environment	
	
	
//*********** how to create jni head file *********** 
// step1: javac filename.java
// step2: cd to src folder
// step3: javah -jni package.file.name.filename


//*********** Set JDK path to eclipse complier *********** 	
// add {JAVA_HOME}/include and {JAVA_HOME}/include/linux to gcc++ complier
//comply command: 
//gcc -I$JAVA_HOME/include -I$JAVA_HOME/include/linux -shared -o libFPGAfuns.so -fPIC filename.cpp
	
	
//*********** include so to java project*********** 
//Add the new created folder named lib to Sourse
//Add *.so file to Natie library location
	
   static {
	   
	      //System.loadLibrary("ISMcore"); // hello.dll (Windows) or libhello.so (Unixes)
   }
	
    public static void main(String[] args) {
        // Prints "Hello, World" to the terminal window.
    	int Ni =250, Nc=10, IL1 = 20000, IL2 = 0, IL3=0, IL4=0, M1=0, M2=1, Np=4, Nwp=200;
    	//lvFPGA.ChangeParameter(Ni, Nc, IL1, IL2, IL3, IL4, M1, M2, Np, Nwp);
    	
    	boolean EnL1 = false, EnL2 = false, EnL3 = false, EnL4 = false;
    	
		

    	lvFPGA.StartImaging(Ni, Nc, EnL1, EnL2, EnL3, EnL4, IL1, IL2, IL3, IL4, M1, M2, Np, Nwp, true);
		Thread thread = new Thread(){
			    public void run(){
			    	int Nimg;
			    	while(!stop){
				    	Nimg = lvFPGA.GetNumImageTaken();
				    	System.out.println(Nimg);
			    	}
			    }
		 };
		thread.start(); 

		
    	
    	lvFPGA.StopImaging();
    	stop = true;
    	
    }
}
