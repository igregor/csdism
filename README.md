# CSDISM

This is a software for superresolution microscopy combining image scanning microscopy (ISM) with a confocal spinning disk (CSD) microscope. We use a Micro-Manager plugin to control the image acquisition and a stand-alone program to reconstruct the super-resolved image.

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.gwdg.de/igregor/csdism.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.gwdg.de/igregor/csdism/-/settings/integrations)


## Name
CSD-ISM --- Confocal Spinning-disk Image-Scanning Microscopy

## Description

The folders contain the following software:
	
for users
----------------
	
  CSDISMusers/Built: contains the MicroManager Plugin for the National Instruments FPGA 7842R and the reconstruction software.
	
  CSDISMusers/MMConfig: contains example configuration files for MicroManager.
	
  MicroManager Plugin (folder: CSDISMusers/Built/ISMplugin)

  Reconstruction software (folder: CSDISMusers/Built/ISMrecon)

for developers
-----------------

  CSDISMdevelopers/Project4OtherPlatforms: contains built files and source code for other NI FPGAs than the 7842R (experimental!).
 	
  CSDISMdevelopers/src: contains the source code for the MicroManager plugin and the reconstruction software.

demo data 
-----------------
  CSDISMusers/Demo data: contains test data.



## Installation

For the user package:

1.  System requirements

   OS: tested on Windows 7 and Windows 10, but any operating system that runs MicroManager 1.4 should work.
	
   software required: MicroManager 1.4
                      Java Runtime Environment (JRE) of version at least 1.8.0_161.
                      jama-1.0.3.jar,	
                      ij-1.52i.jar,
                      FPGA Interface C API 14.0 source file (Copyright (c) 2014 by National Instruments Corporation).
	
   hardware required: National Instruments FPGA

2.  Installation guide
	
   see the "Readme.txt" files
	

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Authors and acknowledgment

Shun Qin, shun.qin@phys.uni-goettingen.de
Sebastian Isbaner, sebastian.isbaner@phys.uni-goettingen.de
Jörg Enderlein, jenderl@gwdg.de
 

## License

see 'license.txt'

## Project status
