CSDISM software for users
-----------------
Built: contains the MicroManager Plugin for the National Instruments FPGA 7842R and the reconstruction software.
MMConfig: contains example configuration files for MicroManager.
Demo data: contains demo data and instructions to use the data with the reconstruction software.

MicroManager Plugin (folder: /Built/ISMplugin)
1.  System requirements
OS: tested on Windows 7 and Windows 10, but any operating system that runs MicroManager 1.4 should work.
software required: MicroManager 1.4
required hardware: National Instruments FPGA
2.  Installation guide
see the "Readme.txt" inside the /Built/ISMplugin folder

Reconstruction software (folder: /Built/ISMrecon)
1.  System requirements
OS: tested on Windows 7 and Windows 10, but any operating system that runs Java should work.
software required: Java Runtime Environment (JRE) of version at least 1.8.0_161.
2.  Installation guide
see the "Readme.txt" inside the /Built/ISMrecon folder
3.  Demo
see the "Readme.txt" inside the /Demo data/ folder
